#import txaio
from twisted.internet import reactor, threads
from twisted.internet.defer import inlineCallbacks
from autobahn.twisted.wamp import ApplicationSession #, ApplicationRunner
from autobahn.wamp.exception import ApplicationError
from autobahn.wamp.types import RegisterOptions
#from wamp import ApplicationSession, ApplicationRunner # copy of stock wamp.py with modified timeouts
import pandas as pd
#import xarray as xr
import numpy as np
#import simplejson
import base64
import zlib
import os
import sys
import platform
from shutil import copyfile

DATA_DIR = '../data/' # TODO: pass in through crossbar config
CSV_FILE = DATA_DIR + 'data.csv'
NPY_FILE = DATA_DIR + 'data.npy'

if platform.system() == 'Linux':
    SHM_FILE = '/dev/shm/data.npy'
else:
    SHM_FILE = '/Volumes/RAMDisk/data.npy' # TODO: hdiutil attach -nomount ram://$((2 * 1024 * SIZE_IN_MB)) ; diskutil eraseVolume HFS+ RAMDisk NAME_OF_DISK
MAX_RECORDS = 1000



# Responds to wamp rpc
class XarrayServerComponent(ApplicationSession):

    @inlineCallbacks
    def onJoin(self, details):

        def filter_dataset(filters=None,
                           start=0,
                           stop=None,
                           indexes=None,
                           fields=None):
            d = threads.deferToThread(_filter_dataset, filters, start, stop, indexes, fields)
            return d


        def _filter_dataset(filters=None,
                            start=0,
                            stop=None,
                            indexes=None,
                            fields=None):
            try:
                #print(reactor.getThreadPool()._queue.qsize())
                r = np.load(SHM_FILE, mmap_mode='r')
                if not filters and not fields == "indexes_only":
                    if indexes:
                        num_results = np.array(indexes)[start:stop].size
                    else:
                        num_results = r['index'][start:stop].size
                    if num_results > MAX_RECORDS:
                        raise ValueError('Requested would return ' + str(num_results) + ' records; please limit request to ' + str(MAX_RECORDS) + ' records.')
                if indexes:
                    r = r[indexes]
                if filters:
                    r = _data_array_query(r, filters)
                filtered_total = r.size;
                if fields and type(fields) is list:
                    r = r[fields]
                r = r[start:stop]

                if fields == "indexes_only":
                    return {'filtered_total': filtered_total, 'indexes': r['index'].tolist()}
                else:
                    if r.size > MAX_RECORDS:
                        raise ValueError('Requested would return ' + str(r.size) + ' records; please limit request to ' + str(MAX_RECORDS) + ' records.')
                    else:
                        #return base64.b64encode(zlib.compress(r.tobytes()))
                        return _format_structured_array_response(r)
            except Exception as e:
                _application_error(e)


        def _structured_array_tobytes(sa, order='struct_of_array'):
            if order == 'array_of_struct':
                new_dtype = [(name, dtype.replace('<', '>').replace('=', '>')) for name, dtype in sa.dtype.descr]
                return np.array(sa, dtype=new_dtype)
            elif order == 'struct_of_array':
                data = []
                for field in sa.dtype.names:
                    col = sa[field]
                    col = col.astype(col.dtype.str.replace('<', '>').replace('=', '>'))
                    data.append(col.tobytes())
                data = b''.join(data)
                return data
            else:
                raise ValueError("Parameter `order` must be either 'struct_of_array' or 'array_of_struct'. Got: '" + str(order) + "'")


        def _format_structured_array_response(sa):
            #records = [{field: sa[i][field].item() for field in sa[i].dtype.names} for i in range(sa.size)]
            #return base64.b64encode(zlib.compress(simplejson.dumps(records, ignore_nan=True)))
            return {'num_rows': sa.size,
                    'col_names': [unicode(name) for name in sa.dtype.names],
                    'col_types': [unicode(sa[name].dtype.name) for name in sa.dtype.names],
                    'item_sizes': [sa[name].dtype.itemsize for name in sa.dtype.names],
                    'data': bytes(zlib.compress(_structured_array_tobytes(sa)))}


        def _application_error(e):
            raise ApplicationError(u'com.example.xarray_server.application_error', e.__class__.__name__, e.message, e.args, e.__doc__)
            

        def _data_array_query(df, filters):
            if not filters:
                return df
            else:
                def _apply_op(op, field, value):
                    if op == '=' or op == 'is':
                        return (df[field] == value)
                    elif op == '<':
                        return (df[field] < value)
                    elif op == '>':
                        return (df[field] > value)
                    elif op == '<=':
                        return (df[field] <= value)
                    elif op == '>=':
                        return (df[field] >= value)
                    elif op == 'between':
                        return ((df[field] >= value[0]) & (df[field] <= value[1]))
                    elif op == 'in':
                        return np.any(df[field][:,np.newaxis] == np.array([value]), axis=1)

                cond = _apply_op(filters[0]['op'], filters[0]['field'], filters[0]['value'])
                for f in filters[1:]:
                    cond &= _apply_op(f['op'], f['field'], f['value'])

                return df[cond]


        def _dataframe_to_structured_array(df):
            col_data = [df.index]
            col_names = ['index']
            col_types = ['i4']
            for name in df.columns:
                column = df[name]
                data = np.array(column)

                if data.dtype.kind == 'O':
                    if all(isinstance(x, basestring) or x is np.nan or x is None for x in data):
                        data[data == np.array([None])] = b''
                        data[np.array([True if str(x) == 'nan' else False for x in data], dtype=np.bool)] = b''
                        data = np.array([x + '\0' for x in data], dtype=np.str)
                col_data.append(data)
                col_names.append(name)
                # javascript cannot natively handle longs
                if str(data.dtype) == 'int64':
                    col_types.append('i4')
                elif str(data.dtype) == 'uint64':
                    col_types.append('u4')
                else:
                    col_types.append(data.dtype.str)
            out = np.array([tuple(data[j] for data in col_data) for j in range(len(df.index))],
                          dtype=[(str(col_names[i]), col_types[i]) for i in range(len(col_names))])
            return out


        #def _structured_array_to_dataset(sa):
        #    return xr.Dataset({field: ('dim_0', sa[field]) for field in sa.dtype.names})


        def _load_data(): 
            print('Loading bogus dataset ...')
            if not os.path.exists(DATA_DIR):
                os.makedirs(DATA_DIR)
            if not os.path.isfile(NPY_FILE):
                if os.path.isfile(CSV_FILE):
                    df = pd.read_csv(CSV_FILE, index_col='index')
                else:
                    # TODO: generate bogus data as list of dicts
                    data_list = []
                    for i in range(100000):
                        data_list.append({'a': i, 'b': float(i), 'c': str(i), 'd': str(i)*20, 'e': i % 2  == 0})
                    df = pd.DataFrame(data_list)
                    del data_list
                    df.to_csv(CSV_FILE, index_label='index')
                sa = _dataframe_to_structured_array(df)
                del df
                np.save(NPY_FILE, sa)
                del sa
            copyfile(NPY_FILE, SHM_FILE)
            #shm = np.load(SHM_FILE, mmap_mode='r')
            #ds = _structured_array_to_dataset(shm)
            print('Done.')

        try:
            yield threads.deferToThread(_load_data)
            yield self.register(filter_dataset,
                                u'com.example.xarray_server.filter_dataset',
                                options=RegisterOptions(invoke=u'roundrobin'))
        except Exception as e:
            print("could not register procedure: {0}".format(e))

        print("Server ready.")

xarray-server
=============

NOTE: This project is early work in progress -- 26/02/2017

**Aim**: Provide a fast server for accessing, searching, and querying array- and table-like data (CSV, `numpy`_ ndarray or `Structured Array`_, `pandas`_ DataFrame, `xarray`_ DataArray/Dataset, or `NetCDF`_), and accompanying javascript client libraries and utilities for fast and minimal bandwidth interaction.

.. _numpy: http://www.numpy.org/
.. _Structured Array: https://docs.scipy.org/doc/numpy/user/basics.rec.html
.. _pandas: http://pandas.pydata.org/
.. _xarray: http://xarray.pydata.org/
.. _NetCDF: https://www.unidata.ucar.edu/software/netcdf/



Live demo
---------

Contains a demo/proof-of-concept of a javascript client accessing and filtering a single table of 100K rows, with dynamic scrolling provided by `js-struct-array`_ and `scroll-preloader`_. The server is implemented on `autobahn.twisted`_ and is run as a `crossbar.io`_ component which serves as `WAMP`_ router.

.. _js-struct-array: https://github.com/chrisbarber/js-struct-array
.. _scroll-preloader: https://github.com/chrisbarber/scroll-preloader
.. _autobahn.twisted: http://autobahn.ws/python/
.. _crossbar.io: http://crossbar.io/
.. _WAMP: http://wamp-proto.org/

View the `live demo`_.

.. _live demo: http://xarray-server.herokuapp.com

How to install
--------------

::

    $ pip install -r requirements.txt
    $ npm install

Running the demo
----------------

::

    $ PORT=8081 crossbar start

Then point your browser to http://localhost:8081
